'use strict';

const crypto = require('crypto');
const config = require('./config');

const algorithm = config.get('ALGORITHM');
const password = config.get('PASSSHIPH');
const iv = Buffer.alloc(16, 0);
const keyLen = Number(config.get('KEYLEN'));
const salt = config.get('SALT');

const key = crypto.scryptSync(password, salt, keyLen);

class Index {
    static async request(body) {
        body = JSON.stringify(body)
        const cipher = crypto.createCipheriv(algorithm, key, iv);

        let encrypted = cipher.update(body, 'utf8', 'hex');
        encrypted += cipher.final('hex');
        return encrypted;
    }

    static async response(body) {
        try {
            const decipher = crypto.createDecipheriv(algorithm, key, iv);

            let decrypted = decipher.update(body, 'hex', 'utf8');
            decrypted += decipher.final('utf8');
            return JSON.parse(decrypted);
        } catch (e) {
            return '';
        }
    }
}

module.exports = Index;
